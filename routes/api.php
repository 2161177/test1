<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::put('users/updatePass/{id}', 'UserController@updatePass');
Route::group(['middleware' => 'waiter'], function () {
    Route::get('restaurant_tables/showFreeTables', 'RestaurantTableController@showFreeTables');
});
Route::get('testepedro', 'MealController@testepedro');
Route::get('invoices/generatePDF/{id}', 'InvoiceController@generatePDF');
Route::get('orders/forcook/{id}', 'OrderController@forCook');
Route::put('orders/forcook/{id}', 'OrderController@updateForCook');
Route::put('users/updateshift/{id}', 'UserController@updateShift');


Route::get('items/showdishes', 'ItemController@showDishes');
Route::get('items/showdrinks', 'ItemController@showDrinks');
Route::get('stats/statsbymonth', 'StatisticsController@statsByMonth');
Route::get('stats/statsbyday', 'StatisticsController@statsByDay');
Route::get('stats/statsbyitem', 'StatisticsController@statsByItem');

Route::group(['middleware' => 'manager'], function () {
    Route::post('auth/register', 'AuthController@register');
    Route::post('users/restore/{id}', 'UserController@restore');
    Route::post('users/block/{id}', 'UserController@block');
    Route::get('meals/showAllMealsActive', 'MealController@showAllMealsActive');
    Route::get('meals/showAllMeals/{state}', 'MealController@showAllMeals');
    Route::get('meals/showAllMealsPaid', 'MealController@showAllMealsPaid');
    Route::get('meals/showAllMealsNotPaid', 'MealController@showAllMealsNotPaid');
    Route::get('meals/showAllMealsTerminated', 'MealController@showAllMealsTerminated');
});

Route::apiResources([
    'users' => 'UserController',
    'invoices' => 'InvoiceController',
    'items' => 'ItemController',
    'meals' => 'MealController',
    'orders' => 'OrderController',
    'tables' => 'RestaurantTableController',
    'restaurant_tables' => 'RestaurantTableController',
    'stats' => 'StatisticsController'
]);

Route::get('orders/showorderspenconf/{id}', 'OrderController@showOrdersPenConf');
Route::get('orders/showordersprep/{id}', 'OrderController@showOrdersPrep');
Route::get('orders/showordersdelivered/{id}', 'OrderController@showOrdersDelivered');
Route::get('meals/showmealsactive/{id}', 'MealController@showMealsActive');
Route::post('items/storeimage/{id}', 'ItemController@storeImage');
Route::post('users/storeimage/{id}', 'UserController@storeImage');
Route::get('orders/showMealOrders/{id}', 'OrderController@showMealOrders');
Route::get('meals/showMealsActive/{id}', 'MealController@showMealsActive');



//Route::get('users/showActive', 'UserController@showActive');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('auth/login', 'AuthController@login');
Route::get('user/verify/{verification_code}', 'AuthController@verifyUser');

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('auth/user', 'AuthController@user');
    Route::post('auth/logout', 'AuthController@logout');


});
Route::group(['middleware' => 'jwt.refresh'], function(){
    Route::get('auth/refresh', 'AuthController@refresh');
});






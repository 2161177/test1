var app = require('http').createServer(function(req,res){

});

var io = require('socket.io')(app);
io.set('origins', '*:*');


app.listen(8080, function(){
    console.log('listening on *:8080');
});


io.on('connection', function (socket) {
    console.log('client has connected (socket ID = '+socket.id+')' );
	socket.on('send_notification_to_manager', function (msg, user) {
		console.log(msg);
		console.log(user);
        	io.sockets.emit('msg_from_server', user + ": " + msg);
	});

	socket.on('orders_updated', function (msg) {
		console.log(msg);
        	io.sockets.emit('orders_updated_from_server', "true");
	});
	socket.on('new_pending_invoice', function (msg) {
		console.log(msg);
        	io.sockets.emit('new_pending_invoice_server', "true");
	});
	socket.on('invoice_updated', function (msg) {
		console.log(msg);
        	io.sockets.emit('invoice_updated_server', "true");
	});
	socket.on('meal_paid', function (msg) {
		console.log(msg);
        	io.sockets.emit('meal_paid_server', "Meal " + msg + ' paid');
	});
	socket.on('meal_terminated', function (msg) {
		console.log(msg);
        	io.sockets.emit('meal_terminated_server', "Meal " + msg + ' terminated');
	});
});
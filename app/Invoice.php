<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Invoice extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state',
        'name',
        'nif'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function items()
    {
        return $this->belongsToMany('App\Item', 'invoice_items');
    }

    public function meal()
    {
        return $this->belongsTo('App\Meal');
    }


    public static function generateInvoice($meal, $orders)
    {
        /*
         * orders(delivered) -> invoice items
         * 5 water orders -> 1 invoice line
         * prefill: meal's date, table number, total price, itens: quantidade, item name, unit price, sub total price
         * fill: NIF, Customer's name required
         * close -> pending -> paid
         * meal -> paid -> generate pdf
         */

        // Create invoice item for each order (unique order names)
        $invoice = new Invoice();
        $invoice->state = 'pending';
        $invoice->meal_id = $meal->id;
        $invoice->date = DB::raw('now()');


        $totalPrice = 0;
        $uniqueItemOrders = [];

        foreach($orders as $order){
            //$uniqueItemOrders['o_' . $order->item_id] += 1;
            $uniqueItemOrders[$order->item_id] = array_key_exists($order->item_id, $uniqueItemOrders) ? $uniqueItemOrders[$order->item_id] + 1 : 1;
        }
        $invoice->total_price = 0;
        $invoice->save();

        foreach($uniqueItemOrders as $key => $quantity){
            $item = Item::find($key);
            if(!$item){
                continue;
            }
            $subtotal = $quantity * $item->price;
            $totalPrice += $subtotal;
            $invoice->items()->attach($key, [
                'invoice_id' => $invoice->id,
                'quantity' => $quantity,
                'unit_price' => $item->price,
                'sub_total_price' => $subtotal
            ]);
        }


        $invoice->total_price = $totalPrice;

        $invoice->save();



    }
}

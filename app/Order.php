<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state',
        'responsible_cook_id',
        'item_id',
        'meal_id',
        'start'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function meal()
    {
        return $this->belongsTo('App\Meal');
    }
    public function item()
    {
        return $this->belongsTo('App\Item');
    }
    public function cook()
    {
        return $this->belongsTo('App\User', 'responsible_cook_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Meal extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state', 'table_number', 'start', 'responsible_waiter_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function orders(){
        return $this->hasMany('App\Order');
    }

    public function waiter(){
        return $this->belongsTo('App\User', 'responsible_waiter_id');
    }

    public function invoice(){
        return $this->hasOne('App\Invoice','meal_id');
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RestaurantTable extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'table_number' => $this->table_number,
            'created_at' => $this->created_at
        ];
    }
}

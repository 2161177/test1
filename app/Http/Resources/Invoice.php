<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Invoice extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'state' => $this->state,
            'nif' => $this->nif,
            'name' => $this->name,
            'date' => $this->date,
            'total_price' => $this->total_price,
            'table_number' => $this->meal()->first()->table_number,
            'items' => $this->items()->withPivot('quantity', 'unit_price', 'sub_total_price')->get(),
            'meal' => $this->meal()->with('waiter')->first(),
        ];
    }
    public function toCleanObject($request)
    {
        return (object) [
            'id' => $this->id,
            'state' => $this->state,
            'nif' => $this->nif,
            'name' => $this->name,
            'date' => $this->date,
            'total_price' => $this->total_price,
            'table_number' => $this->meal()->first()->table_number,
            'items' => $this->items()->withPivot('quantity', 'unit_price', 'sub_total_price')->get(),
            'meal' => $this->meal()->with('waiter')->first(),
        ];
    }
}

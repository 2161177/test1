<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterFormRequest extends FormRequest
{
    protected function validationData()
    {
        return $this->json()->all();
    }

    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'name' => 'required|string|unique:users',
            'username' => 'required|string|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:3|max:20',
        ];
    }
}

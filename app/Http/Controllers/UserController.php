<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Resources\User as UserResource;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{


    /**
     * UserController constructor.
     */
    public function __construct()
    {
        //$this->middleware('manager', ['except' => ['show']]);
        //$this->middleware('manager');
    }

    public function index(Request $request)
    {
        if ($request->has('page')) {
            return UserResource::collection(User::withTrashed()->paginate(25));
        } else {
            return UserResource::collection(User::withTrashed()->get());
        }
    }

    public function show($id)
    {
        return new UserResource(User::find($id));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2|max:255',
            'email' => 'required|email|unique:users,email',
            'age' => 'integer|between:18,75',
            'password' => 'min:3'
        ]);

        $user = new User();
        $user->fill($request->all());
        $user->password = Hash::make($user->password);
        $user->save();
        return response()->json(new UserResource($user), 201);

    }
    public function updateShift(Request $request, $id){

        $data = $request->all();

        $user = User::findOrFail($id);

        if(array_key_exists('last_shift_end', $data) && $end = $data['last_shift_end']){
            $data['last_shift_end'] = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $end)));
        }
        if(array_key_exists('last_shift_start', $data) && $start = $data['last_shift_start']){
            $data['last_shift_start'] = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $start)));
        }
        elseif(array_key_exists('last_shift_start', $data) && $data['last_shift_start'] == null){
            unset($data['last_shift_start']);
        }
        $user->update($data);

        return new UserResource($user);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|min:2|max:255',
            'username' => 'min:2|unique:users,username,' . $id,
            'email' => 'required|email|unique:users,email,'.$id,
        ]);

        $request->offsetUnset('photo_url');


        $data = $request->all();

        $user = User::findOrFail($id);
        if($request->password){
            $user->password = Hash::make($request->password);
        }
        elseif ($request->password == "") {
            $request->password = $user->password;
        }
        if(array_key_exists('last_shift_end', $data) && $end = $data['last_shift_end']){
            $data['last_shift_end'] = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $end)));
        }
        if(array_key_exists('last_shift_start', $data) && $start = $data['last_shift_start']){
            $data['last_shift_start'] = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $start)));
        }
        elseif(array_key_exists('last_shift_start', $data) && $data['last_shift_start'] == null){
            unset($data['last_shift_start']);
        }

        $user->update($data);
        return new UserResource($user);
    }

    public function updatePass(Request $request, $id)
    {
        $request->validate([
            'password' => 'required|string|min:3|max:20',
        ]);

        $user = User::findOrFail($id);
        $user->password = Hash::make($request->password);
//        $user->update(['password' => Hash::make($request->password)]);
        $user->save();
        return new UserResource($user);
    }

    public function storeImage(Request $request, $id = null){

        if(isset($id)){
            $user = User::findOrFail($id);
            $photo_old = $user->photo_url;

            if($photo_old === $request->file){
                return response()->json('', 201);
            } else {
                $filename = basename($request->file('file')->store('public/profiles'));
                Storage::disk('public')->delete('profiles/'.$photo_old);

                $user->photo_url = $filename;
                $user->update();

                return response()->json('Success', 201);
            }
        }

        $filename = basename($request->file('file')->store('public/profiles'));

        return $filename;
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return response()->json(null, 204);
    }

    public function showActive(Request $request)
    {
        $users = DB::table('users')->where('blocked', '=', 0)->get();
        return response()->json($users);
    }

    public function restore(Request $request, $id)
    {
//        $user = User::findOrFail($id);
        $user = User::withTrashed()->where('id', $id)->first();
        if($user->deleted_at != null) {
            $user->restore();
        }
        return response()->json(null, 204);
    }

    public function block($id)
    {
        $user = User::findOrFail($id);
        if($user->blocked == 0) {
            $res = $user->update(array('blocked'=> 1));
            return response()->json($res, 201);
        }
        else {
            $res = $user->update(array('blocked'=> 0));
            return response()->json($res, 202);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Resources\Order as OrderResource;
use App\Order;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
     public function index(Request $request)
    {
        if ($request->has('page')) {
            return OrderResource::collection(Order::paginate(5));
        } else {
            return OrderResource::collection(Order::all());
        }
    }

    public function show($id)
    {
        return new OrderResource(Order::find($id));
    }

    //apenas orders pending ou confirmed para um terminado waiter
    public function showOrdersPenConf(Request $request)
    {
        $orders = DB::table('orders')
            ->join('meals', 'meals.id', '=', 'orders.meal_id')
            ->leftJoin('users', 'users.id', '=', 'orders.responsible_cook_id')
            ->join('items', 'items.id', '=', 'orders.item_id')
            ->select('orders.id', 'orders.state', 'orders.meal_id', 'users.name as user', 'meals.table_number',
                'items.name as item', 'items.photo_url as photo', 'orders.start', 'orders.created_at')
            ->where('meals.responsible_waiter_id', '=', $request->id)
            ->where(function ($query) {
                $query->where('orders.state', '=', 'pending')
                    ->orWhere('orders.state', '=', 'confirmed');
            })
            ->get();

        return response()->json($orders);
    }

    //apenas orders prepared para um terminado waiter
    public function showOrdersPrep(Request $request)
    {
        $orders = DB::table('orders')
            ->join('meals', 'meals.id', '=', 'orders.meal_id')
            ->leftJoin('users', 'users.id', '=', 'orders.responsible_cook_id')
            ->join('items', 'items.id', '=', 'orders.item_id')
            ->select('orders.id', 'orders.state', 'orders.meal_id', 'users.name as user', 'meals.table_number',
                'items.name as item', 'orders.start', 'orders.created_at', 'items.photo_url as photo')
            ->where('meals.responsible_waiter_id', '=', $request->id)
            ->where('orders.state', '=', 'prepared')
            ->get();

        return response()->json($orders);
    }

    //apenas orders delivered para um terminado waiter que o meal ainda não foi pago
    public function showOrdersDelivered(Request $request)
    {
        $orders = DB::table('orders')
            ->join('meals', 'meals.id', '=', 'orders.meal_id')
            ->leftJoin('users', 'users.id', '=', 'orders.responsible_cook_id')
            ->join('items', 'items.id', '=', 'orders.item_id')
            ->select('orders.id', 'orders.state', 'orders.meal_id', 'users.name as user', 'meals.table_number',
                'items.name as item', 'orders.start', 'orders.created_at', 'items.photo_url as photo')
            ->where('meals.responsible_waiter_id', '=', $request->id)
            ->where(function ($query) {
                $query->where('orders.state', '=', 'delivered')
                ->orWhere('orders.state', '=', 'not delivered');
            })
            ->where('meals.state', '=', 'terminated')
            ->get();

        return response()->json($orders);
    }

    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();
        return response()->json(null, 204);
    }

    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        if($order->state === 'prepared'){
            $order->state = 'delivered';
        } else if($order->state === 'pending') {
            $order->state = 'confirmed';
        }
        $order->save();
        return response()->json(null, 204);
    }

    public function updateForCook(Request $request, $id)
    {
        $data = $request->only(['state', 'responsible_cook_id']);

        $order = Order::findOrFail($id);

        $order->update($data);

        return response()->json(null, 204);
    }

    public function store(Request $request)
    {

        $order = new Order();
        $order->fill($request->all());
        $order->start = Carbon::now();
        $order->save();
        return response()->json(new OrderResource($order), 201);

    }

    public function showMealOrders(Request $request, $id) {
        $orders = DB::table('orders')
            ->join('meals', 'meals.id', '=', 'orders.meal_id')
            ->leftJoin('users', 'users.id', '=', 'orders.responsible_cook_id')
            ->join('items', 'items.id', '=', 'orders.item_id')
            ->select('orders.id', 'orders.state', 'orders.meal_id', 'users.name as user', 'meals.table_number',
                'items.name as item', 'orders.start', 'orders.created_at')
            ->where('orders.meal_id', '=', $id)
            ->get();
        return response()->json($orders);
    }

    public function forCook($id){
        $orders = Order::where(function($q) use ($id) {
                $q->where(function($e) use ($id) {
                    $e->where('state', 'in preparation');
                    $e->where('responsible_cook_id', $id);
                })
                ->orWhere('state', 'confirmed');
            })

            ->orderByRaw('state desc, created_at desc')->with('meal')->with('item')->with('cook')
            ->get();
        return response()->json($orders);
    }
}

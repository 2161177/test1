<?php

namespace App\Http\Controllers;

use App\Meal;
use Illuminate\Http\Request;
use App\Http\Resources\Invoice as InvoiceResource;
use App\Invoice;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    public function index(Request $request)
    {
        $invoices = new Invoice();
        if ($request->state) {
            $invoices = $invoices->where('state', '=', $request->state);
        }
        if ($request->date) {
            $invoices = $invoices->where('date', '=', $request->date);
        }

        $invoices = $invoices->with('meal.waiter')->orderBy('id', 'desc')->limit(500)->get();

        if ($request->waiter) {
            foreach ($invoices as $key => $invoice) {
                if (strpos(strtolower($invoice->meal->waiter->name), strtolower($request->waiter)) === false) {
                    $invoices->forget($key);
                }
            }
        }

        return response()->json($invoices);

        /*
           if ($request->has('page')) {
               return InvoiceResource::collection(Invoice::paginate(5));
           } else {
               return InvoiceResource::collection(Invoice::all());
           }
        */
    }

    public function show($id)
    {
        return new InvoiceResource(Invoice::find($id));
    }

    public function store(Request $request)
    {
        /*
        $request->validate([
                'name' => 'required|min:3|regex:/^[A-Za-záàâãéèêíóôõúçÁÀÂÃÉÈÍÓÔÕÚÇ ]+$/',
                'email' => 'required|email|unique:users,email',
                'age' => 'integer|between:18,75',
                'password' => 'min:3'
            ]);
        $user = new User();
        $user->fill($request->all());
        $user->password = Hash::make($user->password);
        $user->save();
        return response()->json(new UserResource($user), 201);
        */
    }

    public function update(Request $request, $id)
    {
        $data = $request->only(['state', 'name', 'nif']);

        $invoice = Invoice::findOrFail($id);


        $invoice->update($data);

        if ($invoice->nif !== null && $invoice->name !== null && $invoice->state === 'paid') {
            Meal::find($invoice->meal_id)->update(['state' => 'paid']);
        }

        if ($invoice->state === 'not paid') {
            Meal::find($invoice->meal_id)->update(['state' => 'not paid']);
            Meal::find($invoice->meal_id)->orders()->where('state', '!=', 'delivered')->update(['state' => 'not delivered']);
        }

        return new InvoiceResource($invoice);
        /*
        $request->validate([
                'name' => 'required|min:3|regex:/^[A-Za-záàâãéèêíóôõúçÁÀÂÃÉÈÍÓÔÕÚÇ ]+$/',
                'email' => 'required|email|unique:users,email,'.$id,
                'age' => 'integer|between:18,75'
            ]);
        $user = User::findOrFail($id);
        $user->update($request->all());
        return new UserResource($user);
*/
    }

    public function destroy($id)
    {
        /*
        $user = User::findOrFail($id);
        $user->delete();
        return response()->json(null, 204);
        */
    }

    public function generatePDF($id)
    {
        $pdf = App::make('dompdf.wrapper');

        $invoice = (new InvoiceResource(Invoice::find($id)))->toCleanObject(null);

        $header =
            "
            <h1>Invoice No. {$invoice->id}</h1>
            <h2>Date. {$invoice->date}</h2>
            <table>
                <tr>
                    <th>
                        Table_number
                    </th>
                    <th>
                        Waiter Name
                    </th>
                    <th>
                        State
                    </th>
                </tr>
                <tr>
                    <td>{$invoice->meal->table_number}</td>
                    <td>{$invoice->meal->waiter->name}</td>
                    <td>{$invoice->state}</td>
                </tr>
            </table>
            <table style='background-color: #ddd'>
                <tr>
                    <td>Name</td>
                    <td>{$invoice->name}</td>            
                </tr>
                <tr>
                    <td>NIF</td>
                    <td>{$invoice->nif}</td>            
                </tr>
            </table>

            ";
        $middle = '<table><tr><th>Name</th><th>Unit Price</th><th>Quantity</th><th>Sub Total Price</th></tr>';
        foreach ($invoice->items as $key => $item) {
            $middle .= "<tr><td>{$item->name}</td><td>{$item->pivot->unit_price}</td><td>{$item->pivot->quantity}</td><td>{$item->pivot->sub_total_price}</td></tr>";
        }
        $middle .= '</table>';

        $middle .= "<div>Final Price: {$invoice->total_price}</div>";

        $final = '<br><br>Gerado por DAD-Project - programa não certificado.';

        $pdf->loadHTML(
            $header . $middle . $final
        );
        /*
         *
                <v-flex xs12>
                    <template>
                        <v-data-table
                            :headers="headers"
                            :items="invoice.items"
                            class="elevation-1"
                            :rows-per-page-items='[50,100,200,{"text":"$vuetify.dataIterator.rowsPerPageAll","value":-1}]'
                        >
                            <template slot="items" slot-scope="props">
                                <td>{{ props.item.name }}</td>
                                <td class="text-xs-right">{{ props.item.pivot.unit_price }}</td>
                                <td class="text-xs-right">{{ props.item.pivot.quantity }}</td>
                                <td class="text-xs-right">{{ props.item.pivot.sub_total_price }}</td>
                            </template>
                        </v-data-table>
                    </template>
                </v-flex>
                <v-flex xs12 sm3>
                    <v-text-field disabled label="Total Price" v-model="invoice.total_price"></v-text-field>
                </v-flex>

                <v-alert v-model="showSuccess" type="success" dismissible>
                    <strong>{{ successMessage }}</strong>
                </v-alert>
         */
        return $pdf->stream();
    }
}

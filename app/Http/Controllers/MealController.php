<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Order;
use App\RestaurantTable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Resources\Meal as MealResource;
use App\Http\Resources\RestaurantTable as RestaurantTableResource;
use App\Meal;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class MealController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page')) {
            return MealResource::collection(Meal::paginate(5));
        } else {
            return MealResource::collection(Meal::all());
        }
    }

    public function show($id)
    {
        return new MealResource(Meal::find($id));
    }

    //apenas meals active para um terminado waiter
    public function showMealsActive(Request $request, $id)
    {
        $meals = DB::table('meals')
            ->select('meals.id', 'meals.state', 'meals.table_number', 'meals.start',
                'meals.total_price_preview',
            DB::raw("(select count(o.state) from orders o where o.meal_id=meals.id and o.state != 'delivered') as state_order"))
            ->where('meals.responsible_waiter_id', '=', $id)
            ->where('meals.state', '=', 'active')
            ->get();

        return response()->json($meals);
    }

    public function showAllMealsActive(Request $request)
    {
        $meals = DB::table('meals')
            ->select('meals.id', 'meals.state', 'meals.table_number', 'meals.start',
                'meals.total_price_preview',
            DB::raw("(select count(o.state) from orders o where o.meal_id=meals.id and o.state != 'delivered') as state_order"))
            ->where('meals.state', '=', 'active')
            ->get();

        return response()->json($meals);
    }

    public function showAllMeals(Request $request, $state)
    {
        $meals = DB::table('meals')
            ->select('meals.id', 'meals.state', 'meals.table_number', 'meals.start',
                'meals.total_price_preview',
                DB::raw("(select count(o.state) from orders o where o.meal_id=meals.id and o.state != 'delivered') as state_order"))
            ->where('meals.state', '=', $state)
            ->limit(100)
            ->get();

        return response()->json($meals);
    }

    public function showAllMealsPaid(Request $request)
    {
        $meals = DB::table('meals')
            ->select('meals.id', 'meals.state', 'meals.table_number', 'meals.start',
                'meals.total_price_preview',
                DB::raw("(select count(o.state) from orders o where o.meal_id=meals.id and o.state != 'delivered') as state_order"))
            ->where('meals.state', '=', 'paid')
            ->get();

        return response()->json($meals);
    }

    public function showAllMealsNotPaid(Request $request)
    {
        $meals = DB::table('meals')
            ->select('meals.id', 'meals.state', 'meals.table_number', 'meals.start',
                'meals.total_price_preview',
                DB::raw("(select count(o.state) from orders o where o.meal_id=meals.id and o.state != 'delivered') as state_order"))
            ->where('meals.state', '=', 'not paid')
            ->get();

        return response()->json($meals);
    }

    public function showAllMealsTerminated(Request $request)
    {
        $meals = DB::table('meals')
            ->select('meals.id', 'meals.state', 'meals.table_number', 'meals.start',
                'meals.total_price_preview',
                DB::raw("(select count(o.state) from orders o where o.meal_id=meals.id and o.state != 'delivered') as state_order"))
            ->where('meals.state', '=', 'terminated')
            ->get();

        return response()->json($meals);
    }
    public function testepedro(Request $request){

//        $meal = Meal::first();
//        $deliveredOrders = Order::take(30)->get();
//
//        $invoice = Invoice::generateInvoice($meal, $deliveredOrders);
        dd(Meal::find(1)->orders()->get());
    }

    public function update(Request $request, $id)
    {
        $meal = Meal::findOrFail($id);
        $orders = Meal::findOrFail($id)->orders()->where('state', '!=','delivered')->get();
        $removed_items = 0;

        if(count($orders) > 0){
            foreach ($orders as $order){
                $item = DB::table('items')->select('price')->where('id', $order->item_id)->first();
                $removed_items += $item->price;
                $order->update(['state' => 'not delivered']);
            }
        }

        if($meal->state === 'active'){
            $meal->state = 'terminated';
            $meal->total_price_preview = $meal->total_price_preview - $removed_items;

            $deliveredOrders = Meal::findOrFail($id)->orders()->where('state', '=','delivered')->get();
            // Meal creates invoice
            Invoice::generateInvoice($meal, $deliveredOrders);
        }
        else if($meal->state === 'terminated' && $request->state === 'not paid') {
//            Meal::find($meal->invoice_id)->update(['state' => 'not paid']);
            $meal->invoice()->first()->update(['state' => 'not paid']);
        }
        $meal->save();

        return response()->json(null, 204);
    }

    public function store(Request $request)
    {
        $request->validate([
                'table_number' => 'required|integer|between:1,30',
                'responsible_waiter_id' => 'required|integer'
            ]);
        app('App\Http\Controllers\RestaurantTableController')->showFreeTables();
        if(! in_array($request->table_number, app('App\Http\Controllers\RestaurantTableController')->showFreeTables())) {
            return response([
                'status' => 'error',
                'error' => 'unavailable.table',
                'message' => 'This Table is not Available'
            ], Response::HTTP_BAD_REQUEST);
        }

        $meal = new Meal();
        $meal->fill($request->all());
        $meal->state = 'active';
        $meal->start = Carbon::now();
        $meal->save();
        return response()->json(new MealResource($meal), 201);
    }

    public function destroy($id)
    {
        /*
        $user = User::findOrFail($id);
        $user->delete();
        return response()->json(null, 204);
        */
    }
}

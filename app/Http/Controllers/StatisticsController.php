<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    public function statsByDay(Request $request)
    {
        $orders_by_cook = DB::table('orders')
            ->join('users', 'users.id', '=', 'orders.responsible_cook_id')
            ->select('users.name',
                DB::raw("COUNT(orders.id)/count(DISTINCT DATE(orders.created_at)) as 'orders_c'"))
            ->whereIn('orders.state', ['delivered', 'not delivered'])
            ->groupBy('orders.responsible_cook_id')
            ->get();

        $orders_by_waiter = DB::table('orders')
            ->join('meals', 'meals.id', '=', 'orders.meal_id')
            ->join('users', 'users.id', '=', 'meals.responsible_waiter_id')
            ->select('users.name',
                DB::raw("COUNT(orders.id)/count(DISTINCT DATE(orders.created_at)) as 'orders_w'"))
            ->whereIn('orders.state', ['delivered', 'not delivered'])
            ->groupBy('meals.responsible_waiter_id')
            ->get();

        $meals_by_waiter = DB::table('meals')
            ->join('users', 'users.id', '=', 'meals.responsible_waiter_id')
            ->select('users.name',
                DB::raw("COUNT(meals.id)/count(DISTINCT DATE(meals.created_at)) as 'meals'"))
            ->whereIn('meals.state', ['paid', 'not paid'])
            ->groupBy('meals.responsible_waiter_id')
            ->get();

        $stats['orders_by_cook'] = $orders_by_cook;
        $stats['orders_by_waiter'] = $orders_by_waiter;
        $stats['meals_by_waiter'] = $meals_by_waiter;

        return response()->json($stats);
    }

    public function statsByMonth(Request $request)
    {
        $by_month = DB::table('meals')
            ->join('orders', 'orders.meal_id', '=', 'meals.id')
            ->select(
                DB::raw("YEAR(orders.created_at) as year, MONTH(orders.created_at) as month, COUNT(distinct orders.id) as orders,
                COUNT(distinct meals.id) as meal, AVG(TIMESTAMPDIFF(MINUTE,meals.start,meals.end)) as meal_avg"))
            ->where(function ($query) {
                $query->whereIn('orders.state', ['delivered', 'not delivered'])
                    ->orWhereIn('meals.state', ['paid', 'not paid']);
            })
            ->groupBy(DB::raw('YEAR(orders.created_at)'), DB::raw('MONTH(orders.created_at)'))
            ->orderBy(DB::raw('YEAR(orders.created_at)'), 'desc')
            ->orderBy(DB::raw('MONTH(orders.created_at)'), 'desc')
            ->get();

        $stats['by_month'] = $by_month;

        return response()->json($stats);
    }

    public function statsByItem(Request $request)
    {
        $by_item = DB::table('orders')
            ->join('items', 'items.id', '=', 'orders.item_id')
            ->select(
                DB::raw("CONCAT(YEAR(orders.created_at), '-' ,MONTH(orders.created_at)) as month, items.name,
                 AVG(TIMESTAMPDIFF(MINUTE,orders.start,orders.end)) as order_avg"))
            ->whereIn('orders.state', ['delivered', 'not delivered'])
            ->groupBy('month', 'items.id')
            ->orderBy('month', 'desc')
            ->orderBy('items.name', 'asc')
            ->get();

        $stats['by_item'] = $by_item;

        return response()->json($stats);
    }
}

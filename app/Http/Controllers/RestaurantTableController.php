<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\RestaurantTable as RestaurantTableResource;
use App\RestaurantTable;
use Illuminate\Support\Facades\DB;

class RestaurantTableController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page')) {
            return RestaurantTableResource::collection(RestaurantTable::paginate(5));
        } else {
            return RestaurantTableResource::collection(RestaurantTable::all());
        }
    }

    public function show($id)
    {
        return new RestaurantTableResource(RestaurantTable::find($id));
    }

    public function destroy($id)
    {
        $table = RestaurantTable::findOrFail($id);
        $table->delete();
        return response()->json(null, 204);
    }

    public function store(Request $request)
    {
        $last_table = RestaurantTable::max('table_number');
        $request->validate(
            ['table_nr' => 'required | min:'.(int)$last_table]
        );
        $table = new RestaurantTable();
        $table->table_number = $request->table_nr;
        $table->fill($request->all());
        $table->save();

        return response()->json(null, 201);
    }

    public function update(Request $request, $id)
    {/*
        $request->validate([
                'name' => 'required|min:3|regex:/^[A-Za-záàâãéèêíóôõúçÁÀÂÃÉÈÍÓÔÕÚÇ ]+$/',
                'email' => 'required|email|unique:users,email,'.$id,
                'age' => 'integer|between:18,75'
            ]);
        $user = User::findOrFail($id);
        $user->update($request->all());
        return new UserResource($user);
*/
    }

    public function showFreeTables()
    {
//        $tables = RestaurantTableResource::collection(RestaurantTable::all());
//        response()->json($tables);
        $tables_active = DB::table('meals')->select('table_number')->where('state', '=', 'active')->pluck('table_number')->toArray();
        $tables = DB::table('restaurant_tables')->whereNotIn('table_number', $tables_active)->pluck('table_number')->toArray();
        return $tables;
    }
}

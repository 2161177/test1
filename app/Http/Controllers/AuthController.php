<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterFormRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function register(RegisterFormRequest $request)
    {
        $name = $request->name;
        $email = $request->email;
        $username = $request->username;
        $password = $request->password;
        $type = $request->type;

//        $user = User::create([
//            'name' => $name,
//            'email' => $email,
//            'username' => $username,
//            'password' => Hash::make($password),
//            'type' => $type
//        ]);
        $user_id = DB::table('users')->insertGetId([
            'name' => $name,
            'email' => $email,
            'username' => $username,
            'password' => Hash::make($password),
            'type' => $type
        ]);
        $user = User::findOrFail($user_id);
        $verification_code = str_random(30); //Generate verification code
        DB::table('user_verifications')->insert(['user_id'=>$user->id,'token'=>$verification_code]);
        $subject = "Please verify your email address.";
        Mail::send('verify', ['name' => $name, 'verification_code' => $verification_code],
            function($mail) use ($email, $name, $subject){
                $mail->from(getenv('FROM_EMAIL_ADDRESS'), "project@dad.pt");
                $mail->to($email, $name);
                $mail->subject($subject);
            });
        return response()->json(['success'=> true, 'message'=> 'Thanks for signing up! Please check your email to complete your registration.']);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');
        $user = User::where('username', $credentials['username']);
        if ($user->exists()) {
            if ($user->get()->first()['email_verified_at'] == null) {
                return response([
                    'status' => 'error',
                    'error' => 'unconfirmed.account',
                    'message' => 'Account Is Not Confirmed'
                ], Response::HTTP_BAD_REQUEST);
            }
            if ($user->get()->first()['blocked'] == 1) {
                return response([
                    'status' => 'error',
                    'error' => 'blocked.account',
                    'message' => 'User Is Blocked'
                ], Response::HTTP_BAD_REQUEST);
            }
        }
        if ( ! $token = JWTAuth::attempt($credentials)) {
            return response([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'message' => 'Invalid Credentials.'
            ], Response::HTTP_BAD_REQUEST);
        }
        return response([
            'status' => 'success'
        ])
            ->header('Authorization', $token);
    }

    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);
        return response([
            'status' => 'success',
            'data' => $user
        ]);
    }

    public function refresh()
    {
        return response([
            'status' => 'success'
        ]);
    }

    public function logout()
    {
        JWTAuth::invalidate();
        return response([
            'status' => 'success',
            'message' => 'Logged out Successfully.'
        ], 200);
    }

    public function verifyUser($verification_code)
    {
        $check = DB::table('user_verifications')->where('token',$verification_code)->first();
        $user = null;
        if(!is_null($check)){
            $user = User::find($check->user_id);
            $message = 'Account already verified..';
            $status = 'success';
            if(!is_null($user->email_verified_at)){
                return view('verified',compact('user', 'message', 'status'));
            }
            DB::table('users')->where('id', $check->user_id)->update(['email_verified_at' => Carbon::now()]);
            DB::table('user_verifications')->where('token',$verification_code)->delete();

            $message = 'You have successfully verified your email address.';
            $status = 'success';
            return view('verified',compact('user', 'message', 'status'));
        }
        $message = 'Verification code is invalid.';
        $status = 'error';
        return view('verified',compact('user', 'message', 'status'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Item as ItemResource;
use App\Item;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ItemController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('page')) {
            return ItemResource::collection(Item::paginate(5));
        } else {
            return ItemResource::collection(Item::all());
        }
    }

    public function show($id)
    {
        return new ItemResource(Item::find($id));
    }

    public function store(Request $request)
    {
        $request->validate([
                'name' => 'required|max:255',
                'description' => 'required|max:255',
                'price' => 'required',
                'type' => 'required'
            ]);

        if(!$request->file){
            return response()->json('Image required', 201);
        } else {
            $filename = $this->storeImage($request);

            $item = new Item();
            $item->fill($request->all());
            $item->photo_url = $filename;
            $item->save();

            return response()->json(new ItemResource($item), 201);
        }
    }

    public function storeImage(Request $request, $id = null){

        if(isset($id)){
            $item = Item::findOrFail($id);
            $photo_old = $item->photo_url;

            if($photo_old === $request->file){
                return response()->json('', 201);
            } else {
                $filename = basename($request->file('file')->store('public/items'));
                Storage::disk('public')->delete('items/'.$photo_old);

                $item->photo_url = $filename;
                $item->update();

                return response()->json('Success', 201);
            }
        }

        $filename = basename($request->file('file')->store('public/items'));

        return $filename;
    }

    public function showDishes(Request $request)
    {
        $dishes = DB::table('items')
            ->where('type', '=', 'dish')
            ->whereNull('deleted_at')
            ->get();

        return response()->json($dishes);
    }

    public function showDrinks(Request $request)
    {
        $drinks = DB::table('items')
            ->where('type', '=', 'drink')
            ->whereNull('deleted_at')
            ->get();

        return response()->json($drinks);
    }

    public function destroy($id)
    {
        $user = Item::findOrFail($id);
        $user->delete();
        return response()->json(null, 204);

    }

    public function update(Request $request, $id)
    {
        $request->offsetUnset('photo_url');

        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'price' => 'required',
            'type' => 'required'
        ]);

        $item = Item::findOrFail($id);
        $item->update($request->all());
        return new ItemResource($item);
    }

}

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/App.vue -> <example-component></example-component>
 */

Vue.component('app', require('./components/App.vue'));
Vue.component('navigation-component', require('./components/NavigationComponent.vue'));
Vue.component('navigation-drawer-component', require('./components/NavigationDrawerComponent'));
Vue.component('menu-component', require('./components/MenuComponent'));
Vue.component('start-shift-component', require('./components/StartShiftComponent'));
Vue.component('menu-item-drink-component', require('./components/MenuItemDrinkComponent'));
Vue.component('menu-item-dish-component', require('./components/MenuItemDishComponent'));
Vue.component('orders-component', require('./components/OrdersComponent'));
Vue.component('orders-waiter-component', require('./components/OrdersWaiterComponent'));
Vue.component('orders-cook-component', require('./components/OrdersCookComponent'));
Vue.component('order-item-pend-component', require('./components/OrderItemPendComponent'));
Vue.component('order-item-prep-component', require('./components/OrderItemPrepComponent'));
Vue.component('order-item-deliv-component', require('./components/OrderItemDelivComponent'));
Vue.component('order-item-cook-component', require('./components/OrderItemCookComponent'));
Vue.component('meals-component', require('./components/MealsComponent'));
Vue.component('meals-waiter-component', require('./components/MealsWaiterComponent'));
Vue.component('meals-manager-component', require('./components/MealsManagerComponent'));
Vue.component('meal-item-component', require('./components/MealItemComponent'));
Vue.component('profile-component', require('./components/ProfileComponent'));
Vue.component('tables-component', require('./components/TablesComponent'));
Vue.component('table-item-component', require('./components/TableItemComponent'));
Vue.component('menu-item-new-component', require('./components/MenuItemNewComponent'));
Vue.component('login-component', require('./components/LoginComponent'));
Vue.component('statistics-component', require('./components/StatisticsComponent'));
Vue.component('stats-by-day-component', require('./components/StatisticsByDayComponent'));
Vue.component('stats-by-month-component', require('./components/StatisticsByMonthComponent'));
Vue.component('stats-by-item-component', require('./components/StatisticsByItemComponent'));
Vue.component('invoices-component', require('./components/InvoicesComponent'));
Vue.component('invoice-one-component', require('./components/InvoiceOneComponent'));
Vue.component('invoice-edit-component', require('./components/InvoiceEditComponent'));
Vue.component('users-component', require('./components/UsersComponent'));
Vue.component('user-edit-component', require('./components/UserEditComponent'));
Vue.component('user-create-component', require('./components/UserCreateComponent'));

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Vuetify from 'vuetify'

Vue.use(Vuetify);
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader

import VueApexCharts from 'vue-apexcharts';

Vue.use(VueApexCharts);
Vue.component('apexchart', VueApexCharts);

import router from './router'
import axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(VueAxios, axios);
axios.defaults.baseURL = '/api';

Vue.router = router;
Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});

import VueSocketio from 'vue-socket.io';

Vue.use(new VueSocketio({
    debug: true,
    connection: 'http://dad.nope.ninja:8080'
}));


const app = new Vue({
    el: '#app',
    data: function () {
        return {
            text: false,
            snackbar: false,
            ordersUpdated: false
        }
    },
    router,
    sockets: {
        connect: function connect() {
            console.log('socket connected (socket ID = ' + this.$socket.id + ')');
        },
        msg_from_server: function msg_from_server(dataFromServer) {
            console.log('Receiving this message from Server: "' + dataFromServer + '"');
            this.text = dataFromServer
            this.snackbar = true
        },
        orders_updated_from_server: function orders_updated_from_server(dataFromServer) {
            console.log('Receiving this message from Server orders_updated_from_server: "' + dataFromServer + '"');
            this.$vueEventBus.$emit('orders-updated', true)
            this.ordersUpdated = true
        },
        new_pending_invoice_server: function new_pending_invoice_server(dataFromServer) {
            console.log('Receiving this message from Server new_pending_invoice: "' + dataFromServer + '"');
            this.$vueEventBus.$emit('new-pending-invoice', true)
        },
        invoice_updated_server: function invoice_updated_server(dataFromServer) {
            console.log('Receiving this message from Server invoice_updated_server: "' + dataFromServer + '"');
            this.$vueEventBus.$emit('invoice-updated', true)
        },
        meal_paid_server: function meal_paid_server(dataFromServer) {
            console.log('Receiving this message from Server meal_paid_server: "' + dataFromServer + '"');
            this.text = dataFromServer
            this.snackbar = true
        },
        meal_terminated_server: function meal_terminated(dataFromServer) {
            console.log('Receiving this message from Server meal_terminated: "' + dataFromServer + '"');
            this.text = dataFromServer
            this.snackbar = true
        }
    }
});

import Datepicker from 'vuejs-datepicker';

Vue.component('Datepicker', Datepicker);

import VueTimeago from 'vue-timeago'

Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
});

Vue.use(require('vue-moment'));

import Vue from 'vue'
import Router from 'vue-router'
import ShiftsComponent from '../components/ShiftsComponent'
import NavigationComponent from '../components/NavigationComponent'
import MenuComponent from "../components/MenuComponent";
import OrdersComponent from "../components/OrdersComponent";
import MealsComponent from "../components/MealsComponent";
import ProfileComponent from "../components/ProfileComponent";
import TablesComponent from "../components/TablesComponent";
import MenuItemNewComponent from "../components/MenuItemNewComponent";
import LoginComponent from "../components/LoginComponent";
import StatisticsComponent from "../components/StatisticsComponent";
import InvoicesComponent from "../components/InvoicesComponent";
import InvoiceEditComponent from "../components/InvoiceEditComponent";
import UsersComponent from "../components/UsersComponent";
import UserEditComponent from "../components/UserEditComponent";
import UserCreateComponent from "../components/UserCreateComponent";

Vue.use(Router);
Vue.prototype.$vueEventBus = new Vue()
export default new Router({
    routes: [
        {
            path: '/',
            name: 'MenuComponent',
            component: MenuComponent
        },
        {
            path: '/1',
            name: 'NavigationComponent',
            component: NavigationComponent
        },
        {
            path: '/orders',
            name: 'OrdersComponent',
            component: OrdersComponent,
            meta: {
                auth: true
            }
        },
        {
            path: '/meals',
            name: 'MealsComponent',
            component: MealsComponent,
            meta: {
                auth: true
            }
        },
        {
            path: '/profile',
            name: 'ProfileComponent',
            component: ProfileComponent,
            meta: {
                auth: true
            }
        },
        {
            path: '/tables',
            name: 'TablesComponent',
            component: TablesComponent,
            meta: {
                auth: true
            }
        },
        {
            path: '/menu',
            name: 'MenuItemNewComponent',
            component: MenuItemNewComponent,
            meta: {
                auth: true
            }
        },
        {
            path: '/statistics',
            name: 'StatisticsComponent',
            component: StatisticsComponent,
            meta: {
                auth: true
            }
        },
        {
            path: '/login',
            name: 'LoginComponent',
            component: LoginComponent,
            meta: {
                auth: false
            }
        },
        {
            path: '/users',
            name: 'UsersComponent',
            component: UsersComponent,
            meta: {
                auth: true
            },
            children: [
                {
                    path: ':id',
                    name: 'UserEditComponent',
                    component: UserEditComponent,
                },
                {
                    path: 'create',
                    name: 'UserCreateComponent',
                    component: UserCreateComponent,
                }
            ]
        },
        {
            path: '/invoices',
            name: 'InvoicesComponent',
            component: InvoicesComponent,
            meta: {
                auth: true
            }
        },
        {
            path: '/invoice/:id',
            name: 'InvoiceEditComponent',
            component: InvoiceEditComponent,
            meta: {
                auth: true
            }
        }
    ]
})
